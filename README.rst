math tools
==========

Description
===========
useful function to compute the root of a second order polynome:
amazing and pretty looking math support

.. math:: a \cdot x^2 + b \cdot x + c = 0

Installation
============

Documentation: Getting started and/or link to documentation.
============================================================
This is a link to the documentation http://fr.wikipedia.org/wiki/documentation

License
=======
Is a *dummy* one for the moment.

Authors
=======
Lots of bright people including but not limited to:

- me
- you
- them